# VPN Server CloudFormation Template

## Usage
### Pre-Requisites:
- An AMI with docker server pre-installed and configured (you can make one this way: https://bitbucket.org/halfsour/packer-aws-dockerhost)
- An S3 bucket

### Steps:
1. Deploy via the console or CLI and supply the necessary Parameters
2. Download the "ikev2-vpn.mobileconfig" file from the S3 bucket you provided in the previous step

### Description:
- This CloudFormation template will utilize an Amazon Linux AMI
- Once an EC2 Instance is created from the provided AMI, cloud-init will pull the [`halfsour/ikev2-vpn-server`](https://registry.hub.docker.com/u/halfsour/ikev2-vpn-server/) docker image and run it
- The resulting run will launch the VPN server within the docker container and write the config file with connectivity information to your S3 bucket
- A signed URL will be sent to the provided email address so that the file can be downloaded and installed it on your iPhone via or your MAC